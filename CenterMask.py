from models.centermask2.centermask.config import get_cfg
from detectron2.engine.defaults import DefaultPredictor
from detectron2.data import MetadataCatalog
import torch

import numpy as np
from shapely.geometry import Polygon
# from skimage.measure import find_contours


class CenterMask:
    def __init__(self, gpu_id, config_file, weights, conf_thresh=0.4):
        self.gpu_id = gpu_id
        self.config_file = config_file
        self.weights = weights
        self.conf_thresh = conf_thresh
        self.cfg = self.setup_cfg()
        self.predictor = DefaultPredictor(self.cfg)
        self.cpu_device = torch.device("cpu")
        self.metadata = MetadataCatalog.get(
            self.cfg.DATASETS.TEST[0] if len(self.cfg.DATASETS.TEST) else (0, "__unused")
        )

    def detect(self, image):
        predictions = self.predictor(image)
        # instances = predictions['instances'].to(self.cpu_device)
        classes = predictions['instances'].pred_classes.cpu().numpy()
        rois = predictions['instances'].pred_boxes.tensor.int().cpu().numpy()
        masks = predictions['instances'].pred_masks.cpu().numpy()
        return masks, rois, classes
        # contours = [find_contours(masks[i, :, :], 0.5) for i in range(masks.shape[0])]
        # return [self._union_figures(contour) for contour in contours], rois

    @staticmethod
    def _union_figures(self, object_contours):
        polygon = Polygon()
        for contour in object_contours:
            if len(contour) < 3:
                continue
            new_polygon = Polygon(np.fliplr(contour))
            if new_polygon.is_valid:
                polygon = polygon.union(new_polygon)
        return polygon

    def detect_norm(self, image):
        predictions = self.predictor(image)
        return predictions

    def setup_cfg(self):
        # load config from file
        cfg = get_cfg()
        # Set config & weights paths
        cfg.merge_from_file(self.config_file)
        cfg.merge_from_list(self.weights)
        # Set score_threshold for builtin models
        cfg.MODEL.RETINANET.SCORE_THRESH_TEST = self.conf_thresh
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = self.conf_thresh
        cfg.MODEL.FCOS.INFERENCE_TH_TEST = self.conf_thresh
        cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = self.conf_thresh
        # Set number of max instances
        cfg.MODEL.FCOS.POST_NMS_TOPK_TEST = 200

        cfg.DATASETS.TEST = "__unused"
        cfg.MODEL.DEVICE = f'cuda:{self.gpu_id}'
        cfg.freeze()
        return cfg


class PolyToMask:
    @staticmethod
    def union_figures(object_contours):
        polygon = Polygon()
        for contour in object_contours:
            if len(contour) < 3:
                continue
            new_polygon = Polygon(np.fliplr(contour))
            if new_polygon.is_valid:
                polygon = polygon.union(new_polygon)
        return polygon
